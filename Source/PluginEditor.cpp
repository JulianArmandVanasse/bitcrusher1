/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "CrushGraphic.h"

//==============================================================================
BitCrusher1AudioProcessorEditor::BitCrusher1AudioProcessorEditor (BitCrusher1AudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
    
    bitDepthSlider.setCentrePosition(getBounds().getWidth() / 2.0 + 30, 30);
    bitDepthSlider.setRange(Range<double> (1.0, 48.0), 1.0);
    bitDepthSlider.setNumDecimalPlacesToDisplay(1);
    bitDepthSlider.setValue(processor.BIT_DEPTH_INIT);
    bitDepthSlider.addListener(this);
    addAndMakeVisible(bitDepthSlider);
    
    bitDepthLabel.setText("Bit Depth", NotificationType::dontSendNotification);
    bitDepthLabel.attachToComponent(&bitDepthSlider, true);
    bitDepthLabel.setJustificationType(Justification::right);
    addAndMakeVisible(bitDepthSlider);
    
    decimateSlider.setCentrePosition(getBounds().getWidth() / 2.0 + 30, 80);
    decimateSlider.setRange(Range<double> (1.0, 50.0), 1.0);
    decimateSlider.setNumDecimalPlacesToDisplay(1);
    decimateSlider.setValue(processor.DECIMATE_INIT);
    decimateSlider.addListener(this);
    addAndMakeVisible(decimateSlider);
    
    decimateLabel.setText("Decimate", NotificationType::dontSendNotification);
    decimateLabel.attachToComponent(&decimateSlider, true);
    decimateLabel.setJustificationType(Justification::right);
    addAndMakeVisible(decimateLabel);
    
    preEmphasisSlider.setCentrePosition(getBounds().getWidth() / 2.0 + 30, 130);
    preEmphasisSlider.setRange(1.0, 50.0);
    preEmphasisSlider.setNumDecimalPlacesToDisplay(1);
    preEmphasisSlider.setValue(processor.DECIMATE_INIT);
    preEmphasisSlider.addListener(this);
    addAndMakeVisible(preEmphasisSlider);
    
    preEmphasisLabel.setText("Pre-emphasis", NotificationType::dontSendNotification);
    preEmphasisLabel.attachToComponent(&preEmphasisSlider, true);
    preEmphasisLabel.setJustificationType(Justification::right);
    addAndMakeVisible(preEmphasisLabel);
    
    crush.setSize((3.0f/4.0f) * getBounds().getWidth(), 100);
    crush.setCentrePosition(getBounds().getWidth() / 2.0f, (getBounds().getHeight() / 2.0f) + 60);
    addAndMakeVisible(crush);
}

BitCrusher1AudioProcessorEditor::~BitCrusher1AudioProcessorEditor()
{
}

//==============================================================================
void BitCrusher1AudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setColour (Colours::white);
    g.setFont (15.0f);
    
    String debug_text = "processor.getBitDepth() = " + String(processor.getBitDepth()) + ", \t slider value = " + String(bitDepthSlider.getValue());
    debug.setText(debug_text, NotificationType::dontSendNotification);
    
}

void BitCrusher1AudioProcessorEditor::resized()
{
    Rectangle<int> area = getBounds();
    int sliderBorder = 30;
    int sliderWidth = area.getWidth() - (3 * sliderBorder);
    int sliderHeight = 50;
    int sliderStartY = sliderBorder;
    int sliderYInc = 50;
    int sliderStartX = area.getWidth() - ((1.0 / 2.0) * sliderWidth) + (2*sliderBorder);
    
    bitDepthSlider.setBounds(sliderStartX, sliderStartY, sliderWidth, sliderHeight);
    decimateSlider.setBounds(sliderStartX, sliderStartY + sliderYInc, sliderWidth, sliderHeight);
    preEmphasisSlider.setBounds(sliderStartX, sliderStartY + (2*sliderYInc), sliderWidth, sliderHeight);
    
    debug.setBounds(100, 100, 100, 100);
    
    crush.setBounds(100, 100, 100, 100);
}

void BitCrusher1AudioProcessorEditor::sliderValueChanged(Slider *slider)
{
    if (slider == &bitDepthSlider)
    {
        processor.setBitDepth(static_cast<int>( bitDepthSlider.getValue()));
        crush.setBitDepth(static_cast<int>( bitDepthSlider.getValue()));
        repaint();
    }
    if (slider == &decimateSlider)
    {
        processor.setDecimate(static_cast<int>(decimateSlider.getValue()));
    }
    if (slider == &preEmphasisSlider)
    {
        processor.setAlpha(static_cast<int>(preEmphasisSlider.getValue()));
    }
}
