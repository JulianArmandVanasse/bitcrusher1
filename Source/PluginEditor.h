/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "CrushGraphic.h"

//==============================================================================
/**
*/
class BitCrusher1AudioProcessorEditor  : public AudioProcessorEditor, public Slider::Listener
{
public:
    BitCrusher1AudioProcessorEditor (BitCrusher1AudioProcessor&);
    ~BitCrusher1AudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    
    void sliderValueChanged(Slider *slider) override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    BitCrusher1AudioProcessor& processor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BitCrusher1AudioProcessorEditor)
    
    CrushGraphic crush;
    
    Slider bitDepthSlider, decimateSlider, preEmphasisSlider;
    Label bitDepthLabel, decimateLabel, preEmphasisLabel;
    Label debug;
};
