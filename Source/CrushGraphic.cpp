/*
  ==============================================================================

    CrushGraphic.cpp
    Created: 17 Jul 2019 6:29:13pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#include "../../../../../../Volumes/Macintosh HD/Users/julianv999/JUCE/JUCE Projects/BitCrusher1/JuceLibraryCode/JuceHeader.h"
#include "CrushGraphic.h"

//==============================================================================
CrushGraphic::CrushGraphic()
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    setSize(200, 20);
}

CrushGraphic::~CrushGraphic()
{
}

void CrushGraphic::paint (Graphics& g)
{
    /* This demo code just fills the component's background and
       draws some placeholder text to get you started.

       You should replace everything in this method with your own
       drawing code..
    */

    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

    g.setColour (Colours::turquoise);
    g.drawRect (getLocalBounds(), 1);   // draw an outline around the component

    generateWaveform();
    for (int i = 0; i < numLines; i++)
    {
        g.drawLine(waveform[i], 2.0);
    }
}

void CrushGraphic::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..

}

void CrushGraphic::generateWaveform()
{
    float x_0 = 0.0f, x_N = 1.0f, dx = (abs(x_0 - x_N) / ((float) numLines));
    float max = 0;
    for (float i = 0.0f; i < numLines; i += 1.0f)
    {
        float x_1 = x_0 + (i * dx);
        float x_2 = x_0 + ((i + 1) * dx);
        float y_1 = sin(2 * 3.141592 * x_1);
        float y_2 = sin(2 * 3.141592 * x_2);

        if (y_1 > max)
            max = y_1;

        y_1 = processor.bitCrush(y_1, bitDepth);
        y_2 = processor.bitCrush(y_2, bitDepth);

        Point<float> p_1 (x_1, y_1);
        Point<float> p_2 (x_2, y_2);

        Line<float> line (p_1, p_2);

        waveform.set((int) i, line);
    }
    
//    AudioBuffer<float> waveData;
//    MidiBuffer m;
//    m.clear();
//    waveData.clear();
//
//    for (float i = 0.0f; i < numLines; i += 1.0f)
//    {
//        float x_1 = x_0 + (i * dx);
//        float y_1 = sin(2 * 3.141592 * x_1);
//
//        waveData.getWritePointer(0)[(int) i] = y_1;
//    }
//
//    processor.processBlock(waveData, m);
//
//    for (float i = 0.0f; i < numLines - 1; i += 1.0f)
//    {
//        float x_1 = x_0 + (i * dx);
//        float x_2 = x_0 + ((i + 1) * dx);
//        float y_1 = waveData.getSample(0, (int) i);
//        float y_2 = waveData.getSample(0, (int) (i + 1.0f));
//
//        Line<float> line (x_1, y_1, x_2, y_2);
//
//        waveform.set((int) i, line);
//    }

    
    for (float i = 0; i < numLines; i++)
    {
        Line<float> line = waveform[i];
        line.applyTransform(AffineTransform::scale(getBounds().getWidth() / abs(x_0 - x_N), getBounds().getHeight() / max / 2.0f));
        line.applyTransform(AffineTransform::translation(0, getBounds().getHeight() / 2.0f));
        waveform.set(i, line);
    }
}

void CrushGraphic::setBitDepth(int newBitDepth)
{
    bitDepth = newBitDepth;
}

int CrushGraphic::getBitDepth()
{
    return bitDepth;
}
