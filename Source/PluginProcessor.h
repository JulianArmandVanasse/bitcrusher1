/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/**
*/
class BitCrusher1AudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    BitCrusher1AudioProcessor();
    ~BitCrusher1AudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    void setBitDepth(int newBitDepth);
    void setDecimate(int newDecimate);
    void setAlpha(float newAlpha);
    
    int getBitDepth();
    int getDecimate();
    float getAlpha();
    
    float bitCrush(float sample, int bd);
    float preEmphasis(float sample);
    
    int BIT_DEPTH_INIT {4};
    int DECIMATE_INIT {5};
    
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BitCrusher1AudioProcessor)
    
    
    int bitDepth {BIT_DEPTH_INIT};
    
    // decimation
    int decimate {DECIMATE_INIT};
    int count {0};
    Array<float> lastSampleValue;
    
    // pre-emphasis
    float alpha {1.0};
};
