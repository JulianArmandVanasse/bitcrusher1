/*
  ==============================================================================

    CrushGraphic.h
    Created: 17 Jul 2019 6:29:13pm
    Author:  Julian Vanasse

  ==============================================================================
*/

#pragma once

#include "../../../../../../Volumes/Macintosh HD/Users/julianv999/JUCE/JUCE Projects/BitCrusher1/JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

//==============================================================================
/*
*/
class CrushGraphic    : public Component
{
public:
    CrushGraphic();
    ~CrushGraphic();

    void paint (Graphics&) override;
    void resized() override;

    void generateWaveform();
    void drawWaveform();
    
    void setBitDepth(int newBitDepth);
    int getBitDepth();
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CrushGraphic)
    
    Array<Line<float>> waveform;
    int numLines {128};
    BitCrusher1AudioProcessor processor;
    int bitDepth {processor.BIT_DEPTH_INIT};
    
    
};
