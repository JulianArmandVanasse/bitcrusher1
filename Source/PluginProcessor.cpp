/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <cmath>

//==============================================================================
BitCrusher1AudioProcessor::BitCrusher1AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

BitCrusher1AudioProcessor::~BitCrusher1AudioProcessor()
{
}

//==============================================================================
const String BitCrusher1AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool BitCrusher1AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool BitCrusher1AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool BitCrusher1AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double BitCrusher1AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int BitCrusher1AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int BitCrusher1AudioProcessor::getCurrentProgram()
{
    return 0;
}

void BitCrusher1AudioProcessor::setCurrentProgram (int index)
{
}

const String BitCrusher1AudioProcessor::getProgramName (int index)
{
    return {};
}

void BitCrusher1AudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void BitCrusher1AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void BitCrusher1AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool BitCrusher1AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void BitCrusher1AudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

//    int currentBitDepth = bitDepth.getNextValue();
    int currentBitDepth = bitDepth;
    
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        
        for (int sample = 0; sample < buffer.getNumSamples(); sample++)
        {
            if (count % decimate == 0)
            {
                float value = buffer.getReadPointer(channel)[sample];
                value = preEmphasis(value);
                value = bitCrush(value, currentBitDepth);
                value = value * (1.0f / sqrt(alpha));
                buffer.getWritePointer(channel)[sample] = value;
                lastSampleValue.set(channel, value);
                count = 1;
            }
            else
            {
                buffer.getWritePointer(channel)[sample] = lastSampleValue[channel];
            }
            count ++;
        }
    }
}

void BitCrusher1AudioProcessor::setBitDepth(int newBitDepth)
{
//    bitDepth.setValue(newBitDepth);
    bitDepth = newBitDepth;
}

void BitCrusher1AudioProcessor::setDecimate(int newDecimate)
{
    decimate = newDecimate;
}

void BitCrusher1AudioProcessor::setAlpha(float newAlpha)
{
    alpha = newAlpha;
}

int BitCrusher1AudioProcessor::getBitDepth()
{
//    return bitDepth.getTargetValue();
    return bitDepth;
}

int BitCrusher1AudioProcessor::getDecimate()
{
    return decimate;
}

float BitCrusher1AudioProcessor::getAlpha()
{
    return alpha;
}

float BitCrusher1AudioProcessor::bitCrush(float sample, int bd)
{
    int x = static_cast<int> (sample * pow(2.0, bd));
    return ((float)x) / pow(2.0, bd);
}

float BitCrusher1AudioProcessor::preEmphasis(float sample)
{
    return tanh(sample * alpha);
}

//==============================================================================
bool BitCrusher1AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* BitCrusher1AudioProcessor::createEditor()
{
    return new BitCrusher1AudioProcessorEditor (*this);
}

//==============================================================================
void BitCrusher1AudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void BitCrusher1AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new BitCrusher1AudioProcessor();
}
